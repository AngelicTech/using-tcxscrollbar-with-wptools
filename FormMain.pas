unit FormMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, dxSkinsCore, dxSkinBlueprint, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary,
  dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinPumpkin,
  dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinTheBezier, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue, cxScrollBar,
  WPCTRMemo, WPRuler, WPRTEDefs, WPCTRRich;

type
  TForm1 = class(TForm)
    VertScrollBar: TcxScrollBar;
    HorizScrollBar: TcxScrollBar;
    Editor: TWPRichText;
    HorizRuler: TWPRuler;
    VertRuler: TWPVertRuler;
    procedure EditorUpdateExternScrollbar(Sender: TWPCustomRtfEdit;
        ScrollBar: TScrollStyle; Range, Page, Pos: Integer);
    procedure HorizScrollBarScroll(Sender: TObject; ScrollCode:
        TScrollCode; var ScrollPos: Integer);
    procedure VertScrollBarScroll(Sender: TObject; ScrollCode:
        TScrollCode; var ScrollPos: Integer);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.EditorUpdateExternScrollbar(Sender:
    TWPCustomRtfEdit; ScrollBar: TScrollStyle; Range, Page, Pos:
    Integer);
var
  AScrollBar: TcxScrollBar;
  AVisible: Boolean;
begin
  //Determine correct scrollbar to modify.
  case ScrollBar of

    //Modify horizontal scrollbar.
    TScrollStyle.ssHorizontal: AScrollBar := HorizScrollBar;

    //Modify vertical scrollbar.
    TScrollStyle.ssVertical  : AScrollBar := VertScrollBar;

  else
    //No scrollbar to modify.
    AScrollBar := NIL;
  end;

  //Check if we have a scrollbar reference to modify.
  if (NOT Assigned(AScrollBar)) then
    Exit; //None, so exit method.

  //Scrollbar is visible if page size is less than the range.
  AVisible := (Page < Range);

  //Set scrollbar visibility.
  AScrollBar.Visible := AVisible;

  //If scrollbar is not visible then exit method.
  if (NOT AVisible) then
    Exit;

  //Set scrolbar range, page size and position.
  AScrollBar.Max := Range;
  AScrollBar.PageSize := Page;
  AScrollBar.Position := Pos;
end;

procedure TForm1.HorizScrollBarScroll(Sender: TObject; ScrollCode:
    TScrollCode; var ScrollPos: Integer);
var
  ANewPos: Integer;
  AScrHorizMult: Single;
begin
  //Get scroll horizontal multiplier from editor.
  AScrHorizMult := Editor.ScrollHorzMultiplicator;

  //Calculate new position in editor.
  ANewPos := Round(ScrollPos / AScrHorizMult);

  //Set new editor horizontal position.
  Editor.LeftOffset := ANewPos;
end;

procedure TForm1.VertScrollBarScroll(Sender: TObject; ScrollCode:
    TScrollCode; var ScrollPos: Integer);
var
  ANewPos: Integer;
  AScrVertMult: Single;
begin
  //Get scroll vertical multiplier from editor.
  AScrVertMult := Editor.ScrollVertMultiplicator;

  //Calculate new position in editor.
  ANewPos := Round(ScrollPos / AScrVertMult);

  //Set new editor vertical position.
  Editor.TopOffset := ANewPos;
end;

end.
